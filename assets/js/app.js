$(function() {

	// Get the form.
	var form = $('#ajax-contact');

	// Get the messages div.
	var formMessages = $('#form-messages');

	// Set up an event listener for the contact form.
	$(form).submit(function(e) {
		// Stop the browser from submitting the form.
		e.preventDefault();

		// Serialize the form data.
		var formData = $(form).serialize();

		// Submit the form using AJAX.
		$.ajax({
			type: 'POST',
			url: $(form).attr('action'),
			data: formData
		})
		.done(function(response) {
			// Make sure that the formMessages div has the 'success' class.
			$(formMessages).removeClass('error');
			$(formMessages).addClass('success');

			// Set the message text.
			$(formMessages).text(response);

			// Clear the form.
			$('#name').val('');
			$('#email').val('');
			$('#message').val('');

			grecaptcha.reset(contactWidget);
		})
		.fail(function(data) {
			// Make sure that the formMessages div has the 'error' class.
			$(formMessages).removeClass('success');
			$(formMessages).addClass('error');

			// Set the message text.
			if (data.responseText !== '') {
				$(formMessages).text(data.responseText);
			} else {
				$(formMessages).text("Oops! Something went wrong and we couldn't send your message. Please send us an email and we'll fix this ASAP :)");
			}

			grecaptcha.reset(contactWidget);
		});

	});

	// the same procedure above is applied to the register form
	var registerForm = $('#ajax-register');
	var registerMessage = $('#register-messages');

	$(registerForm).submit(function(e) {
		e.preventDefault();

		var registerData = $(registerForm).serialize();

		$.ajax({
			type: 'POST',
			url: $(registerForm).attr('action'),
			data: registerData
		})
		.done(function(response) {
			// if the registration was successfull we simply clear the form
			$(registerMessage).removeClass('error');
			$(registerMessage).addClass('success');

			// Set the message text.
			$(registerMessage).text(response);

			// Clear the form.
			$('#first_name').val('');
			$('#last_name').val('');
			$('#address').val('');
			$('#zip').val('');
			$('#state').val('');
			$('#country').val('');
			$('#email').val('');
			$('#package').val('');
			$('#shirt_size').val('');
			$('#discount').val('');
			$('#terms_conditions').val('');

			grecaptcha.reset(registerWidget);
		})
		.fail(function(data) {
			// if we fail we have to give the user feedback
			$(registerMessage).removeClass('success');
			$(registerMessage).addClass('error');

			// Set the message text.
			if (data.responseText !== '') {
				$(registerMessage).text(data.responseText);
			} else {
				$(registerMessage).text("Oops! Something went wrong and we couldn't send your message. Please send us an email and we'll fix this ASAP :)");
			}

			grecaptcha.reset(registerWidget);
		});
	});
});
