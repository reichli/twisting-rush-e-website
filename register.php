<?php

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require 'assets/PHPMailer/src/Exception.php';
    require 'assets/PHPMailer/src/PHPMailer.php';
    require 'assets/PHPMailer/src/SMTP.php';

    // My modifications to mailer script from:
    // http://blog.teamtreehouse.com/create-ajax-contact-form
    // Added input sanitizing to prevent injection
    // Further modified by Thomas Reichel

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // check reCaptcha (= twisting_rushe_dev)
        $recaptchaKey = '6Legv5wUAAAAAKVaj_r2QaJ4I5IdlZq8FDnS5qlf';
        $recaptchaToken = $_POST['g-recaptcha-response'];

        if (!$recaptchaToken) {
            http_response_code(400);
            echo "You're a robot wizard, Harry! (= reCaptcha not solved, please try again)";
            exit;
        }

        $tokenVerfyResponse = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$recaptchaKey."&response=".$recaptchaToken);
        $responseKeys = json_decode($tokenVerfyResponse, true);

        if (intval($responseKeys["success"]) !== 1) {
            http_response_code(400);
            echo "You're a robot wizard, Harry! (= reCaptcha not solved, please try again)";
            exit;
        }

        // get the form fields and remove whitespace.
        $firstName = strip_tags(trim($_POST["first_name"]));
        $lastName = strip_tags(trim($_POST["last_name"]));
        $name = $firstName . " " . $lastName;
        $address = strip_tags(trim($_POST["address"]));
        $zip = strip_tags(trim($_POST["zip"]));
        $state = strip_tags(trim($_POST["state"]));
        $country = strip_tags(trim($_POST["country"]));
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);      
        $package = strip_tags(strip_tags(trim($_POST["package"])));
        $shirtSize = strip_tags(trim($_POST["shirt_size"]));
        $discount = strip_tags(trim($_POST["discount"]));
        
        // do some error checking
        if (!isset($_POST["terms_conditions"]))
        {
            http_response_code(400);
            echo "Please read and accept the terms and conditions.";
            exit;            
        }
        
        if (empty($_POST["terms_conditions"]) OR $_POST["terms_conditions"] != 'on') {
            http_response_code(400);
            echo "Please read and accept the terms and conditions.";
            exit;
        }

        if (empty($firstName) OR empty($lastName) OR empty($address) OR empty($zip) OR empty($state) OR
            empty($country) OR empty($email) OR empty($package) OR empty($shirtSize))
        {
            http_response_code(400);
            echo "Please fill out the whole form.";
            exit;
        }

        // set up PHPMailer
        $mail = new PHPMailer;

        $mail->isSMTP();
        //$mail->SMTPDebug = 3;
        $mail->Host = 'cpanel01.gigaplanet.si';
        $mail->SMTPAuth = true;
        $mail->Username = 'info@twistingrushe.si';
        $mail->Password = 'rushe2019';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->CharSet = 'UTF-8';
        $mail->isHTML(true);	

        // set up the email
        $replacements = array();

        $replacements['first_name'] = $firstName;
        $replacements['last_name'] = $lastName;
        $replacements['address'] = $address;
        $replacements['zip'] = $zip;
        $replacements['state'] = $state;
        $replacements['country'] = $country;
        $replacements['email'] = $email;
        $replacements['package'] = $package;        
        $replacements['shirt'] = $shirtSize;
        $replacements['discount'] = $discount;
        
        $email_content = file_get_contents("assets/templates/register.html");
        
        foreach ($replacements as $key => $value)
        {
            $email_content = str_replace('[['.$key.']]', $value, $email_content);
        }

        $mail->setFrom('info@twistingrushe.si', 'TwistingRushe');
        $mail->addAddress('info@twistingrushe.si', 'TwistingRushe');
        $mail->addBCC('ziga@twistingrushe.si', 'Ziga Sporin');
        $mail->addBCC($email, $name);

        $mail->Subject = "[Twisting Rush-e] Registration from $name";
        $mail->Body = $email_content;
        $mail->AltBody = strip_tags($email_content);
        
        // Send the email.
        $success = $mail->send();
        if ($success) {
            http_response_code(200);
            echo "Thank you for your registration! You will receive a copy of the registration email. If you don't, please check your spam folder or contact us directly.";
        } else {
            http_response_code(500);
            echo "Oops! Something went wrong and we couldn't send your message. Please send us an email and we'll fix this ASAP :)";
            //echo $mail->ErrorInfo;
        }

    } else {
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }
?>
