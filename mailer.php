<?php

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require 'assets/PHPMailer/src/Exception.php';
    require 'assets/PHPMailer/src/PHPMailer.php';
    require 'assets/PHPMailer/src/SMTP.php';

    // My modifications to mailer script from:
    // http://blog.teamtreehouse.com/create-ajax-contact-form
    // Added input sanitizing to prevent injection
    // Further modified by Thomas Reichel

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // check reCaptcha (= twisting_rushe_dev)
        $recaptchaKey = '6Legv5wUAAAAAKVaj_r2QaJ4I5IdlZq8FDnS5qlf';
        $recaptchaToken = $_POST['g-recaptcha-response'];

        if (!$recaptchaToken) {
            http_response_code(400);
            echo "You're a robot wizard, Harry! (= reCaptcha not solved, please try again)";
            exit;
        }

        $tokenVerfyResponse = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$recaptchaKey."&response=".$recaptchaToken);
        $responseKeys = json_decode($tokenVerfyResponse, true);

        if (intval($responseKeys["success"]) !== 1) {
            http_response_code(400);
            echo "You're a robot wizard, Harry! (= reCaptcha not solved, please try again)";
            exit;
        }

        // get the form fields and remove whitespace.
        $name = strip_tags(trim($_POST["name"]));
		$name = str_replace(array("\r","\n"), array(" "," "), $name);
        $email = filter_var(strip_tags(trim($_POST["email"]), FILTER_SANITIZE_EMAIL));
        $message = strip_tags(trim($_POST["message"]));

        // check that data was sent to the mailer.
        if (empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            http_response_code(400);
            echo "Please fill out all fields.";
            exit;
        }

        // set up PHPMailer
        $mail = new PHPMailer;

        $mail->isSMTP();
        //$mail->SMTPDebug = 3;
        $mail->Host = 'cpanel01.gigaplanet.si';
        $mail->SMTPAuth = true;
        $mail->Username = 'info@twistingrushe.si';
        $mail->Password = 'rushe2019';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->CharSet = 'UTF-8';
        $mail->isHTML(true);	

        // set up the email
        $replacements = array();

        $replacements['name'] = $name;
        $replacements['email'] = $email;
        $replacements['message'] = $message;
        
        $email_content = file_get_contents("assets/templates/contact.html");
        
        foreach ($replacements as $key => $value)
        {
            $email_content = str_replace('[['.$key.']]', $value, $email_content);
        }

        $mail->setFrom('info@twistingrushe.si', 'TwistingRushe');
        $mail->addAddress('info@twistingrushe.si', 'TwistingRushe');
        $mail->addBCC('ziga@twistingrushe.si', 'Ziga Sporin');
        $mail->addBCC($email, $name);

        $mail->Subject = "[Twisting Rush-e] Contact from $name";
        $mail->Body = $email_content;
        $mail->AltBody = strip_tags($email_content);
        
        // Send the email.
        $success = $mail->send();
        if ($success) {
            http_response_code(200);
            echo "Thank You! Your message has been sent. You will receive a copy of the contact email. If you don't, please check your spam folder or contact us directly.";
        } else {
            http_response_code(500);
            echo "Oops! Something went wrong and we couldn't send your message. Please send us an email and we'll fix this ASAP :)";
            //echo $mail->ErrorInfo;
        }

    } else {
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }
?>